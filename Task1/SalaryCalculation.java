import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Locale;

class Employee {
    private String name;
    private double salary;
    private LocalDate startDate;

    public Employee(String name, LocalDate startDate, double salary) {
        this.name = name;
        this.startDate = startDate;
        this.salary = salary;
    }

    public Employee(String name, int startYear, Month startMonth, int startDay, double salary) {
        this.name = name;
        this.startDate = LocalDate.of(startYear, startMonth, startDay);
        this.salary = salary;
    }

    public int getStartYear() {
        return startDate.getYear();
    }

    public int getStartDay() {
        return startDate.getDayOfMonth();
    }

    public String getName() {
        return name;
    }

    public Month getStartMonth() {
        return startDate.getMonth();
    }

    public double getSalary() {
        return salary;
    }

    // Metode untuk menghitung THR
    public double calculateTHR() {
        Period period = Period.between(startDate, LocalDate.now());
        int monthsWorked = period.getMonths() + 1;

        // Jika bulan kerja kurang dari 12 bulan, berikan THR sesuai dengan bulan
        // bekerja
        if (monthsWorked < 12 && monthsWorked > 0) {
            return (salary / 12) * monthsWorked;
        } else {
            return salary;
        }
    }
}

class SalaryThread extends Thread {
    private Employee employee;
    private int months;

    public SalaryThread(Employee employee, int months) {
        this.employee = employee;
        this.months = months;
    }

    // Metode untuk mengonversi nilai double ke format mata uang Rupiah
    public static String doubleToRupiah(double amount) {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        return currencyFormat.format(amount);
    }

    @Override
    public void run() {
        try {
            String folderPath = "Task1";
            File folder = new File(folderPath);
            FileWriter fileWriter = new FileWriter(folder + "/Employee-" + employee.getName() + ".txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);

            // Menentukan bulan pertama gaji setelah bulan mulai kerja
            Month firstSalaryMonth = employee.getStartMonth().plus(1);

            for (int i = 1; i <= months; i++) {
                StringBuilder sbTHR = new StringBuilder();
                StringBuilder sbMonthly = new StringBuilder();
                // Menghitung THR
                double thr = 0;
                if (firstSalaryMonth.name() == "MARCH") {
                    thr = employee.calculateTHR();
                    printWriter
                            .println(sbTHR.append(i).append(". Salary ").append(firstSalaryMonth.name()).append(" : ")
                                    .append(doubleToRupiah(employee.getSalary() + thr))
                                    .append(" (monthly + THR) - start month : ")
                                    .append(employee.getStartMonth()).append(" ").append(employee.getStartYear()));
                } else {
                    printWriter.println(
                            sbMonthly.append(i).append(". Salary ").append(firstSalaryMonth.name()).append(" : ")
                                    .append(doubleToRupiah(employee.getSalary())).append(" (monthly)"));
                }
                // Pindah ke bulan berikutnya
                firstSalaryMonth = firstSalaryMonth.plus(1);
            }
            printWriter.close();
            System.out.println("Generate " + "Employee-" + employee.getName() + ".txt" + " succsess");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

public class SalaryCalculation {
    public static void main(String[] args) {
        // Membuat objek Employee untuk setiap karyawan
        Employee employee1 = new Employee("Ayu", 2024, Month.FEBRUARY, 1, 5000000);
        Employee employee2 = new Employee("Biagi", 2024, Month.MARCH, 1, 8000000);
        Employee employee3 = new Employee("Cipta", LocalDate.of(2024, 4, 1), 12000000);

        // Membuat thread untuk setiap karyawan
        SalaryThread thread1 = new SalaryThread(employee1, 12);
        SalaryThread thread2 = new SalaryThread(employee2, 12);
        SalaryThread thread3 = new SalaryThread(employee3, 12);

        // Menjalankan thread untuk menghasilkan file gaji karyawan
        thread1.start();
        thread2.start();
        thread3.start();
    }
}

