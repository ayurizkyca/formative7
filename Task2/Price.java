package Task2;
class Price {
    private int id;
    private double amount;
    private String valuta;
    private int productId;

    public Price(int id, double amount, String valuta, int productId) {
        this.id = id;
        this.amount = amount;
        this.valuta = valuta;
        this.productId = productId;
    }

    public int getId() {
        return id;
    }

    public double getAmount() {
        return amount;
    }

    public String getValuta() {
        return valuta;
    }

    public int getProductId() {
        return productId;
    }

    public void setAmount(double d) {
        this.amount = amount;
    }
}
